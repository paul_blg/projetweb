<!DOCTYPE html>

<html>

    <head>
        <title> Index </title>
        <meta charset="utf-8" />
        <!--Lien pour CSS -->
        <link href="css/index.css" rel="stylesheet" />
        <!-- Créer une icône dans l'onglet-->
        <link rel="icon" type="image/png" href="image/logi.png" />
        <!-- Lien pour le javascrpit -->
        <script type="text/javascript" src="js/fonction.js"> </script>
    </head>

    <body>

    	<div id="page_container">
            <div id="menu-bandeau">
                <div id="bandeau">

                </div>
                <div id="menu">
                    <li>
                        <u></u>
                        <u></u>
                        <u></u>
                        <u></u>
                    </li>
                </div>
            </div>
        	<div id="presentation">
                <p>
                    Voici un site permettant de se connecter ou s'inscrire afin de poster ses photos, ou bien de regarder les photos d'autres personnes.
                </p>
            </div>
            <div id="formulaire">
        		<form action="pass.php" method="post">
                    <table id="mytable">
                        <tr>
                		    <td> <p> Login: </p> </td>
                            <td> <input type="text" name="login" id="login" /> </td>
                        </tr>
                        <tr>
                            <td> <p> Password: </p> </td>
                            <td> <input type="password" name="pass" id="password" /> <br /> <br /> </td>
                        </tr>
                        <tr>
                            <td colspan="2" id="connexion_button" > <input type="submit" value="Connexion" onclick="connexion()" /> </td>
                        </tr>
                        <tr>
                            <td colspan="2"> <p> <a href=""> Inscrivez-vous! </a> </p> </td>
                        </tr>
                    </table>
        		</form>
        	</div>

        <footer>
            <p>Contact us !</p>
            <a href="https://www.facebook.com/"> <img src="images/facebook.png"> </a>
            <a href="https://www.youtube.com/"> <img src="images/youtube.png"> </a>
            <a href="https://twitter.com/?lang=fr"> <img src="images/twitter.png"> </a>
            
            <p>Copyrights! All rights reserved! Created by Paul Bellanger, Thomas Guisnel and Alan Lachkar</p>
            
        </footer>
    	</div>

    </body>

</html>