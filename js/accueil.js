/*Ouvre le popup lorsqu'on clique sur une photo*/


function open_popup(id_photo) {
    
    document.getElementById("photo_infos").style.display = "block";
    document.getElementById("image_avec_infos").src = document.getElementById(id_photo).src;
    
}

function hide(id) {
    
    document.getElementById(id).style.display = "none";
}

function show(id) {
    
    document.getElementById(id).style.display = "block";
}

function affichage_box(string_to_display)
{
    
    alert(string_to_display);
}

function get_exif(){
    var src = document.getElementById("image_avec_infos").src;
    
    if (src == "") {
        document.getElementById("table_infos_exif").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("table_infos_exif").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","display_exif.php?src="+src,true);
        xmlhttp.send();
    }
}

function get_description()
{
      var src = document.getElementById("image_avec_infos").src;
    
    if (src == "") {
        document.getElementById("texte_description").innerHTML = "";
        return;
    } else { 
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("texte_description").innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","get_description.php?src="+src,true);
        xmlhttp.send();
    }
    
}



function change(sens, id_photo)
               {
                   var i;
                   if (sens == 'up')
                   {
                   for (i=1;i<4; i++)
                   {
                       document.getElementById("img1_"+i).src=document.getElementById("img2_"+i).src;
                   
                   }
                       
                   }
                   if (sens == 'down')
                   {
                   for (i=1;i<4; i++)
                   {
                       document.getElementById("img2_"+i).src=document.getElementById("img1_"+i).src;
                   
                   }
                       
                   }
               
               
               }
