function inscrivezVous(){

	document.getElementById("formulaire_connexion").style.display="none";

	document.getElementById("formulaire_inscription").style.display="block";

}

function connectezVous(){

	document.getElementById("formulaire_inscription").style.display="none";

	document.getElementById("formulaire_connexion").style.display="block";

}


/******************Validation du formulaire**************************************************/


function valider_inscription(){
	document.getElementById("errordiv").innerHTML = "";
	var complet=true;

	complet &=validerNom();
	complet &=validerPrenom();
	complet &=validerEmail();
	complet &=validerMdp1();
	complet &=validerMdp2();

	if(complet==false){
		document.getElementById("errordiv").innerHTML = "Certains champs sont incomplets.<br\>";
		document.getElementById("errordiv").style.display="block";
		return false;
	}
	else
		return true;	
}

function validerNom(){
	var nom = document.getElementById("nom");
	var style = nom.style;

	if(nom.value==='' || !nom.value.match(/^[A-Za-z]+$/)){
		nom.style.borderColor="red";
		return false;
	}
	nom.style = style;
	return true;
}
function validerPrenom(){
	var prenom = document.getElementById("prenom");
	var style = prenom.style;
	if(prenom.value==='' || !prenom.value.match(/^[A-Za-z]+$/)){
		prenom.style.borderColor="red";
		return false;
	}
	prenom.style = style;
	return true;
}
function validerEmail(){
	var mail = document.getElementById("mail");
	var style = mail.style;

	if(mail.value===''){
		mail.style.borderColor="red";
		return false;
	}
	mail.style=style;
	return true;
}
function validerMdp1(){
	var mdp = document.getElementById("new_password");
	var style = mdp.style;
	if(mdp.value===''){
		mdp.style.borderColor="red";
		return false;
	}
	mdp.style = style;
	return true;
}
function validerMdp2(){
	var mdp = document.getElementById("password_again");
	var check = document.getElementById("new_password");

	var style = mdp.style;
	if(mdp.value != check.value || mdp.value===''){
		//document.getElementById("errordiv").innerHTML += "Les mots de passe sont différents.";
		mdp.style.borderColor="red";
		return false;
	}
	mdp.style = style;
	return true;
}
