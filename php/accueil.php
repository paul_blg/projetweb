<html>
  <head>
    <link href="../css/accueil.css" rel="stylesheet" type="text/css" >
    <meta charset="utf-8" />
    <script type="text/javascript" src="../js/accueil.js"></script> 
    <script type="text/javascript" src="../js/profil.js"> </script>

    <!-- Créer une icône dans l'onglet-->
    <link rel="icon" type="image/png" href="../images/logo4.png" />
    <!-- Titre dans l'onglet et dans la barre de recherches -->
    <title> Accueil </title>

  </head> 

<?php
  session_start();
  
  if($_SESSION["admin"] == true)
    include 'entete_avec_conversion.php';
  else
    include 'entete_sans_conversion.php';
?>  
    <body>
        
       
        
        <div id="corps_page">
            <div id="bloc_principal">
                <div id="fleche_haut" onclick="change('up',0);">&#8648;</div>
                <div id="table_presentation">
                    <div class="table_row">
                        <div class="table_cell" ><img src="../images/cheval_bleu.jpg" id="img1_1" onclick="open_popup('img1_1');"  ></div>
                        <div class="table_cell"><img src="../images/chien_perche.jpg" id="img1_2" onclick="open_popup('img1_2');" ></div>
                        <div class="table_cell"><img src="../images/bouffe4.jpg" id="img1_3" onclick="open_popup('img1_3');" ></div>
                    </div>
                    <div class="table_row">
                        <div class="table_cell"><img src="../images/hot_chocolate.jpg" id="img2_1" onclick="open_popup('img2_1');" ></div>
                        <div class="table_cell"><img src="../images/lac_soleil.jpg" id="img2_2" onclick="open_popup('img2_2');" ></div>
                        <div class="table_cell"><img src="../images/montagne.jpg" id="img2_3" onclick="open_popup('img2_3');" ></div>
                    </div>

                </div>
                <div id="fleche_bas" onclick="change('down',0);">&#8650;</div>
                
            </div>
            
            <div id="photo_infos">
            <div id="croix" onclick='hide("exif_photo"); show("description_photo"); hide("photo_infos");'>✖</div>
            <div id="table_infos">
                <div class="table_row">
                    <div class="table_cell">
                        <img id="image_avec_infos" src="../images/tallinn.jpg">
                    </div>
                        
                    
                    
                    <div class="table_cell">
                    
                      <div id="description_photo">
                          <div class="onglets">
                              <div class="table_row">
                                  <div id="cell_description">Description Photo</div>
                                  <div id="cell_exif_unselected" onclick='hide("description_photo"); show("exif_photo"); get_exif();'>Infos Exif</div>
                              </div>
                          </div>
                          
                          <div id="texte_description"></div>
                      </div>
                       <div id="exif_photo">
                           <div class="onglets">
                               <div class="table_row">
                                   <div id="cell_description_unselected" onclick='hide("exif_photo"); show("description_photo"); get_description();'>Description Photo</div>
                                   <div id="cell_exif">Infos Exif</div>
                               </div>
                            </div>
                           <div id="table_infos_exif">
                                                       
                          
                       
                          
                           
                       </div>
                        
                            </div>
                 
                    
               
            </div>
                
            </div>
                </div></div></div>
          
        <?php
          include 'footer.php';
        ?>
    </body>
    
</html>



