<?php
    
    $dbname = "web";
    $db_login="root";
    $db_pass="";

    try{
        $bdd = new PDO("mysql:host=localhost;
                        dbname=$dbname;
                        charset=utf8",
                        $db_login, $db_pass);
        // $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $type = $_GET["q"];
    
        switch ($type) {
            case 'connexion':
                $login = isset($_POST["login"])?$_POST["login"]:"";
                $mdp = isset($_POST["pass"])?$_POST["pass"]:"";
                
                $sql = "SELECT Nom, Prenom, Mail, Mot_de_passe, ID, Inscription_validee, Admin FROM utilisateur";

                $results = $bdd->query($sql);
                $connect = 0;
                $user_id = 0;
                $Inscription_validee = 0;
                $admin = 0;
                $msg = "";

                while ($log = $results->fetch()) {
                    if($log['Mail'] == $login and $log['Mot_de_passe'] == $mdp and $log['Inscription_validee'] == 1){
                        $connect = 1;
                        $user_id = $log['ID'];
                        $Inscription_validee = 1;
                        $admin = $log['Admin'];
                        $nom = $log['Nom'];
                        $prenom = $log['Prenom'];
                        break;    
                    }
                    else{
                        if($log['Inscription_validee'] == 0){
                            $msg = '<br/>'."Votre compte n'a pas encore été validé par un administrateur.";
                        }
                        if($log['Mail'] != $login or $log['Mot_de_passe'] != $mdp){
                            $msg = '<br/>'."\nLogin ou mot de passe érroné !";
                        }
                    }
                }

                
                $inscris = -1;
                break;

            case 'inscription':

                $nom = isset($_POST["nom"])?$_POST["nom"]:"";
                $prenom = isset($_POST["prenom"])?$_POST["prenom"]:"";
                $mail = isset($_POST["login"])?$_POST["login"]:"";
                $password = isset($_POST["pass"])?$_POST["pass"]:"";
                $password_new = isset($_POST["pass_new"])?$_POST["pass_new"]:"";

                $error = "";
                $inscris = 1;
                $connect = -1;

                $sql = "SELECT Mail FROM utilisateur WHERE Mail = '$mail'";

                $results = $bdd->query($sql);



                if($nom == ""){
                    $error.="Le champs 'Nom' est vide !<br/>"; 
                    $inscris = 0;
                }
                if($prenom == ""){
                    $error.="Le champs 'Prénom' est vide !<br/>";
                    $inscris = 0;
                }
                if($mail == ""){
                    $error.="Le champ 'Adresse mail' est vide !<br/>";
                    $inscris = 0;
                }
                if($password == ""){
                    $error.="Le champs 'Mot de passe' est vide !<br/>";
                    $inscris = 0;
                }
                if($password_new == "" or $password_new != $password){
                    $error.="Erreur lors de la confirmation du mot de passe.<br/>";
                    $inscris = 0;
                }
                while($trouve = $results->fetch()){
                    if($trouve['Mail'] == $mail){
                        $error.="Cette adresse est déjà utilisée !";
                        $inscris = 0;
                    }
                }
                if($inscris==1)
                    $type='connexion';
                break;
            
            default:
                # code...
                break;
        }
        
    }
    catch(Exception $e){
        echo $e->getMessage();
        return;
    }
?>    
<!DOCTYPE html>
<html>
    <head>
        <title> Index </title>
        <meta charset="utf-8" />
        <!--Lien pour CSS -->
        <link href="../css/index.css" rel="stylesheet" />
        <!-- Créer une icône dans l'onglet-->
        <link rel="icon" type="../image/png" href="../image/logi.png" />
        <!-- Lien pour le javascrpit -->
        <script type="text/javascript" src="../js/index.js"> </script>
        <script type="text/javascript" src="../js/profil.js"> </script>
    </head>

    <body>

    	<div id="page_container">
        	<div id="presentation">
                <h1>
                    PICTURE PARADISE
                </h1>
            </div>
            <div id="formulaire_connexion" 
            <?php 
                if($type=='connexion') 
                    echo "class='visible'>";
                if($type == 'inscription')
                    echo "class='invisible'>";

                    if($connect == -1){

                    }
                    else{
                        if($connect == 0 or $connect == 2)
                            echo "$msg";
                        else{
                            session_start();
                            $_SESSION["login"] = $login;
                            $_SESSION["user_id"] = $user_id;
                            $_SESSION["admin"] = $admin;
                            $_SESSION["nom"] = $nom;
                            $_SESSION["prenom"] = $prenom;
                            header('location: ../php/accueil.php');
                            exit();
                        }
                    }

                ?>
                <form action="index.php?q=connexion" method="post">
                    <table id="mytable">
                        <tr id="tr_login">
                            <td> <input type="text" name="login" placeholder="Email address" id="login" /> </td>
                        </tr>
                        <tr id="tr_password">
                            <td> <input type="password" name="pass" placeholder="Password" id="password" /> <br /> <br /> </td>
                        </tr>
                        <tr id="tr_connexion">
                            <td colspan="2"  > <input type="submit" value="Connexion" id="connexion_button" title="Connexion" onclick="connexion()" />
                            <br /> </td>
                            
                         </tr>
                        <tr id="tr_lien_inscription">
                            <td colspan="2" > <p> <a href="#" id="lien_inscription" title="Inscrivez-vous!" onclick="inscrivezVous()"> Inscrivez-vous! </a> </p> </td>
                        </tr>
                    </table>
                </form>

        	</div>
        	
        	<div id="formulaire_inscription"
            <?php
                if($type=='connexion') 
                    echo "class='invisible'>";
                if($type == 'inscription')
                    echo "class='visible'>";
            ?>
                <div id="errordiv"></div>
                <?php
                    if($inscris == 0){
                        echo "$error";
                    }
                ?>
                <form action="index.php?q=inscription" onsubmit="return valider_inscription()" method="post">
                    <table id="mytable">
                        <tr id="tr_nom">
                            <td> Nom </br> <input type="text" name="nom" placeholder="Nom" id="nom" /> </td>
                        </tr>
                        <tr id="tr_prenom">
                            <td> Prénom </br> <input type="text" name="prenom" placeholder="Prénom" id="prenom" /> </td>
                        </tr>
                        <tr id="tr_mail">
                            <td> Adresse email </br> <input type="text" name="login" placeholder="Adresse mail " id="mail" />  </td>
                        </tr>
                        <tr id="tr_password">
                            <td> Mot de passe </br> <input type="password" name="pass" placeholder="Password" id="new_password" />  </td>
                        </tr>
                        <tr id="tr_password_again">
                            <td> Confirmez le mot de passe </br> <input type="password" name="pass_new" placeholder="Password" id="password_again" /><br /> <br />  </td>
                        </tr>                                
                        <tr id="tr_inscription">
                            <td colspan="2"  > <input type="submit" value="S'inscrire" id="inscription_button" title="Inscription" /> <br /> </td>
                        </tr>
                        <tr id="tr_lien_connexion">
                            <td colspan="2" > <p> <a href="#" id="lien_connexion" title="Déjà membre ?" onclick="connectezVous()"> Déjà membre ? </a> </p> </td>
                        </tr>
                
                    </table>
                </form>
                
        	</div>

        <?php
            include 'footer.php';
        ?>
    	</div>
        <?php
            if($inscris == 1){

                $sql = "INSERT INTO utilisateur (Nom, Prenom, Mail, Mot_de_passe) VALUES ('$nom', '$prenom', '$mail', '$password')";
                $bdd->exec($sql);
                
                echo '<script>alert("Bonjour'." $prenom".'\n\nNous vous remercions pour votre inscription à \'Picture Paradise\'.\n\nVous pourrez vous connectez à votre compte après confirmation d\'un administrateur.\n\nA bientôt sur \'Picture Paradise\' !")</script>';

            }
        ?>
    </body>

</html>