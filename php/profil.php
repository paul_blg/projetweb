<!DOCTYPE html>

<html>

 <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="../css/profil.css" />

    <!-- Créer une icône dans l'onglet-->
    <link rel="icon" type="../image/png" href="../images/logo4.png" />
    <!-- Lien pour le javascrpit -->
    <script type="text/javascript" src="../js/profil.js"> </script>
	<!-- Titre dans l'onglet et dans la barre de recherches -->
    <title> Page Mon Profil </title>
</head>
<?php
	session_start();
	
	if($_SESSION["admin"] == true)
		include 'entete_avec_conversion.php';
	else
		include 'entete_sans_conversion.php';
?>	
	<body>
		<div id="contenu_page">
			

			<div id="milieu">
				<div id="colonne_gauche">
					<nav>
						<h3> Données personelles </h3>
						<ul>
							<li> Prénom Nom: </li>
							<li> Âge: </li>
							<li> Ville: </li>
							<li> Pseudo: </li>
						</ul>
						<h3 id="parametre"> Paramètres </h3>
						<ul>
							<li> Ajouter des photos </li>
							<!-- <li> Gérer des photos </li> -->
							<li> Supprimer des photos </li>
							<li> Déconnexion </li>
						</ul>
					</nav>

				</div>
				<div id="colonne_milieu">
					<div id="fleche">
							<img src="../images/fleche_haut.png">
						</div>
					<div id="liste_albums">
						

						<div id="album">
							<img src="../images/photo1.png">
							<p> Album 1 </p>
						</div>

						<div id="album">
							<img src="../images/photo2.png">
							<p> Album 2 </p>
						</div>
						
						<div id="album">
							<img src="../images/photo3.png">
							<p> Album 3 </p>
						</div>
						
						<div id="album">
							<img src="../images/photo4.png">
							<p> Album 4 </p>
						</div>
						
						<div id="album">
							<img src="../images/photo5.png">
							<p> Album 5 </p>
						</div>
						
						<div id="album">
							<img src="../images/photo6.png">
							<p> Album 6 </p>
						</div>

						
					</div>
					<div id="fleche">
							<img src="../images/fleche_bas.png">
						</div>
				</div>
				<div id="colonne_droite">
					<div id="info_album">

						<nav>
							<h3> Informations sur l'album </h3>
							<ul>
								<li> Titre: </li>
								<li> Lieu: </li>
								<li> Date de création: </li>
								<li> Nombre de photos: </li>
								<li> Propriétaire: </li>
							</ul>
						</nav>
					</div>
				</div>
				<div id="colonne_milieu2">
					<div id="fleche2">
							<img src="../images/fleche_gauche.png">
					</div>

					<div id="photo">
						<img src="../images/bouffe1.jpg">
					</div>

					<div id="photo">
						<img src="../images/bouffe2.jpg">
					</div>

					<div id="photo">
						<img src="../images/bouffe3.jpg">
					</div>

					<div id="photo">
						<img src="../images/bouffe4.jpg">
					</div>

					<div id="photo">
						<img src="../images/bouffe5.jpg">
					</div>
						
					<div id="photo">
						<img src="../images/bouffe6.jpg">
					</div>

					<div id="fleche2">
							<img src="../images/fleche_droite.png">
					</div>
				</div>
			</div>

			<?php
				include 'footer.php';
			?>
		</div>
	</body>

</html>